from fastapi import FastAPI

from connections.database_sqlite import engine, Base

from api import api_router

Base.metadata.create_all(bind=engine)

from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.instrumentation.fastapi import FastAPIInstrumentor
from opentelemetry.instrumentation.sqlalchemy import SQLAlchemyInstrumentor

resource = Resource.create(attributes={"service.name": "api-users"})
otlp = OTLPSpanExporter(endpoint="localhost:4317", insecure=True)
span_processor = BatchSpanProcessor(span_exporter=otlp)
jaeger_provider = TracerProvider(resource=resource)
jaeger_provider.add_span_processor(span_processor=span_processor)

app = FastAPI()

SQLAlchemyInstrumentor().instrument(tracer_provider=jaeger_provider, engine=engine, enable_commenter=True, commenter_options={})
FastAPIInstrumentor().instrument_app(app=app, tracer_provider=jaeger_provider)

app.include_router(api_router)
