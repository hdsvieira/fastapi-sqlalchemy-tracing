from sqlalchemy.orm import Session

from models.users import Users

class UsersRepository:
    @staticmethod
    def find_all(db: Session) -> list[Users]:
        return db.query(Users).all()

    @staticmethod
    def save(db: Session, curso: Users) -> Users:
        if curso.id:
            db.merge(curso)
        else:
            db.add(curso)
        db.commit()
        return curso

    @staticmethod
    def find_by_id(db: Session, id: int) -> Users:
        return db.query(Users).filter(Users.id == id).first()

    @staticmethod
    def exists_by_id(db: Session, id: int) -> bool:
        return db.query(Users).filter(Users.id == id).first() is not None

    @staticmethod
    def delete_by_id(db: Session, id: int) -> None:
        curso = db.query(Users).filter(Users.id == id).first()
        if curso is not None:
            db.delete(curso)
            db.commit()
