from schemas.users import (
    UserRequest,
    UserResponse
)
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status,
    Response
)
from sqlalchemy.orm import Session
from models.users import Users
from repository.users import UsersRepository
from connections.database_sqlite import get_db

router = APIRouter()

@router.post("/api/usuario", response_model=UserResponse, status_code=status.HTTP_201_CREATED)
def create(request: UserRequest, db: Session = Depends(get_db)):
    curso = UsersRepository.save(db, Users(**request.model_dump()))
    return UserResponse.model_validate(curso)

@router.get("/api/usuario", response_model=list[UserResponse])
def find_all(db: Session = Depends(get_db)):
    cursos = UsersRepository.find_all(db)
    return [UserResponse.model_validate(curso) for curso in cursos]

@router.get("/api/usuario/{id}", response_model=UserResponse)
def find_by_id(id: int, db: Session = Depends(get_db)):
    curso = UsersRepository.find_by_id(db, id)
    if not curso:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Curso não encontrado"
        )
    return UserResponse.model_validate(curso)

@router.delete("/api/usuario/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_by_id(id: int, db: Session = Depends(get_db)):
    if not UsersRepository.exists_by_id(db, id):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Curso não encontrado"
        )
    UsersRepository.delete_by_id(db, id)
    return Response(status_code=status.HTTP_204_NO_CONTENT)

@router.put("/api/usuario/{id}", response_model=UserResponse)
def update(id: int, request: UserRequest, db: Session = Depends(get_db)):
    if not UsersRepository.exists_by_id(db, id):
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Curso não encontrado"
        )
    curso = UsersRepository.save(db, Users(id=id, **request.model_dump()))
    return UserResponse.model_validate(curso)