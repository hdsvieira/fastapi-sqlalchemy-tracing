from pydantic import BaseModel

class UserBase(BaseModel):
    name: str

class UserRequest(UserBase):
    ...

class UserResponse(UserBase):
    id: int

    class Config:
        from_attributes = True
